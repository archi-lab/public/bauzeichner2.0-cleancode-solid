# Bauzeichner2.0-CleanCode-SOLID

This is an example repo to demonstrate the refactoring of source code according to
Clean Code and SOLID principles. 

It uses an example shown in the [ArchiLab Youtube Channel](https://www.youtube.com/channel/UC29euiLjp5m-hPoU3QP3nGA).
See [SOLID Overview](https://www.youtube.com/watch?v=kQDStoasH-Q) and subsequent videos, and
the [Clean Code Introduction Video](https://www.youtube.com/watch?v=2G20nqeHAn8).

The refactoring steps are outlined in more detail on 
[the ArchiLab Exercise Page on Clean Code and SOLID](https://www.archi-lab.io/exercises/st2/bauzeicher20-cleancode-solid.html).
